clear;

X= imread('person.bmp');
Y= rgb2gray(X);
Y=im2double(Y);

Y=Y*255;   %but still double

imshow(Y/255);

myfun=@dct2;  %define an inline function

Z= blkproc(Y,[8 8],myfun);   %block processing: apply myfun on each block.

load JPEGMTX;
JPEGMTX= 1*JPEGMTX;

fun2 = @(x) x./JPEGMTX;

ZZ=blkproc(Z,[8 8],fun2);

ZZZ=round(ZZ);

fun3 = @(x) x.*JPEGMTX;
ZZZZ=blkproc(ZZZ, [8 8], fun3);

%inverse DCT2
fun4=@idct2;
YY=blkproc(ZZZZ,[8 8],fun4);


figure();

subplot(2,1,1), imshow(Y/255);
subplot(2,1,2), imshow(YY/255);
