clear


kk=32;   %Number of clusters
%taking a person image
x1=imread('person.bmp');
x=im2double(x1);

sss = size(x);

A1 = im2col(x(:,:,1),[sss(1) sss(2)],'distinct');
A2 = im2col(x(:,:,2),[sss(1) sss(2)],'distinct');
A3 = im2col(x(:,:,3),[sss(1) sss(2)],'distinct');

A=[A1 A2 A3];


opts = statset('MaxIter',500);
[idx CodeBook] = kmeans(A,kk,'EmptyAction','singleton','Options',opts);
lsize=size(idx);

%The codebook is already designed: CodeBook  and idx is the index for each
%pixel
Indexed = col2im(idx, [sss(1) sss(2)], [sss(1) sss(2)], 'distinct');
%this is the indexed image where the index is stored in each pixel

%Now we generate the actual color for each pixel:
for i=1:sss(1)
    for j=1:sss(2)
        iii = Indexed(i,j);
        for m=1:3
            III(i,j,m) = CodeBook(iii,m);
        end
    end
end

subplot(2,1,1), imshow(x1);
subplot(2,1,2), imshow(III);
figure
imshow(Indexed,CodeBook)



