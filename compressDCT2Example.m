clear;

%x=imread('baboon.bmp');
x=imread('baboon.bmp');



%Transform the image into gray (for many applications gray is sufficient)
y= rgb2gray(x);
y=im2double(y);    %from unsigned integer (0-255) to double (0-1)
%This is neccessary for image transforms.

%The 2-dimensional DCT

ydct=dct2(y);   



figure(1);
subplot(2,1,1), imshow(y);
subplot(2,1,2), imshow(abs(ydct));

size1=size(ydct);
ksize1=size1(1)*size1(2)
%This gives the total number of coefficients before compression

%We take only a small portion of the DCT coefficients (K*K)
K=20;    %We can play with K and see the result each time
yzeros=zeros(size1(1),size1(2));

ydct1=yzeros;

ydct1(1:K,1:K)=ydct(1:K,1:K);

%The size reduction (compression%) 
compression=100-100*(K*K)/ksize1

ycomp=idct2(ydct1);      %Inverse DCT2
figure(2);
subplot(2,1,1), imshow(y);
subplot(2,1,2), imshow(ycomp);
